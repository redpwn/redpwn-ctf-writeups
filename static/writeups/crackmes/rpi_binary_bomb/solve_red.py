alphabet = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789'
r = [0x6B8B4567, 0x327B23C6, 0x643C9869]
password = ''
for i in range(0, 0x12 + 1):
    password += alphabet[r[2] & 0x1f]
    r[2] = (r[2] >> 5) | (r[1] << 0x1b);
    r[1] = (r[1] >> 5) | (r[0] << 0x1b);
    r[0] = r[0] >> 5;
print(password) # 'KDG3DU32D38EVVXJM64'
