import ctypes

solution = 0x40475194
graph_addr = 0x0804c160
graph = [
    0x0804c19c, 0x47bbfa96,
    0x0804c178, 0x0804c214,
    0x50171a6e, 0x0804c1b4,
    0x0804c1d8, 0x23daf3f1,
    0x0804c1a8, 0x0804c19c,
    0x634284d3, 0x0804c1c0,
    0x0804c1f0, 0x344c4eb1,
    0x0804c1fc, 0x0804c1cc,
    0x0c4079ef, 0x0804c214,
    0x0804c178, 0x425ebd95,
    0x0804c184, 0x0804c1cc,
    0x07ace749, 0x0804c1a8,
    0x0804c1e4, 0x237a3a88,
    0x0804c184, 0x0804c1f0,
    0x4b846cb6, 0x0804c184,
    0x0804c214, 0x1fba9a98,
    0x0804c1c0, 0x0804c19c,
    0x3a4ad3ff, 0x0804c1c0,
    0x0804c184, 0x16848c16,
    0x0804c178, 0x0804c190,
    0x499ee4ce, 0x0804c1b4,
    0x0804c1c0, 0x261af8fb,
    0x0804c184, 0x0804c1cc,
    0x770ea82a, 0x0804c1fc
]

def addr_to_node_index(addr):
    return (addr - graph_addr) // 4

# String must be newline-terminated
def check(password):
    try:
        node_index = 0
        berry = ctypes.c_uint32(graph[node_index + 1]).value
        for i in range(0xe + 1):
            end = False
            letter = password[i]
            if letter == 'L':
                node_index = addr_to_node_index(graph[node_index])
            elif letter == 'R':
                node_index = addr_to_node_index(graph[node_index + 2])
            else:
                end = True
            if end:
                break
            berry = ctypes.c_uint32(berry ^ graph[node_index + 1]).value
        return berry == solution
    except:
        return False

# 'LLRR'
def crack_password():
    len = 0
    i = 0
    while len <= 0xe:
        n = i
        if n == 0:
            password = 'L'
        else:
            password = ''
            while n != 0:
                password += 'L' if n % 2 == 0 else 'R'
                n //= 2
        if check(password + '\n'):
            return password
        i += 1

print(crack_password())
