---
title: Tiebreaker
author: redpwn
date: '2018-08-27'
ctfs:
  - pactf2018
categories:
  - pyjail
---

## Solve script

[solve.py](solve.py)

## Writeup

We are given a server, and our task is to make the server respond to an HTTP GET request with our team name, "meephackerz".

After a bit of testing, we recognize that the server runs in python2, and, and anything we submit is eval'd twice, with the setback that no alphanumeric characters are allowed.

Python2 lets us represent arbitrary characters as `"%c" % <num>`. We now must simply represent `'c'` and any arbitrary integer using non alphanumeric characters.

We know that `+True` evaluates to `1`, and thus we can represent any integer through arithmetic operations involving `+({}=={})`'s. The easiest way to do this is repeatedly add it to itself.

We also know that surrounding `'¬'` with backticks makes `'\\xc2\\xac'`, which lets us use the letter `c`.

We combine these ideas to create a payload the evaluates to `[c for c in (1).__class__.__base__.__subclasses__() if 'warning' in c.__name__][0]()._module.__builtins__['__import__']('os').system('/bin/sh')`, to give us a shell. Finally, we install `php` and create an index file with our team name.
