redpwn is a Capture-the-Flag team (and group of Arch Linux users) primarily composed of high school students.

Current members:
* arinerron
* blevy
* Bilbin
* dns
* fishy15
* ItzSomebody
* NotDeGhost
* piplupdragon
* Tortelloni
* thepiercingarrow
* Tux

[ArchWiki](https://wiki.archlinux.org/)
